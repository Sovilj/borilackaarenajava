package borilackaArena.carolija;


public abstract class Carolija {

    private String nazivCarolije;
    private int maxNanesenaSteta;
    private int minNanesenaSteta;

    public String getNazivCarolije() {
        return nazivCarolije;
    }

    public void setNazivCarolije(String nazivCarolije) {
        this.nazivCarolije = nazivCarolije;
    }

    public int getMaxNanesenaSteta() {
        return maxNanesenaSteta;
    }

    public void setMaxNanesenaSteta(int maxNanesenaSteta) {
        this.maxNanesenaSteta = maxNanesenaSteta;
    }

    public int getMinNanesenaSteta() {
        return minNanesenaSteta;
    }

    public void setMinNanesenaSteta(int minNanesenaSteta) {
        this.minNanesenaSteta = minNanesenaSteta;
    }

}

