package borilackaArena;

import borilackaArena.sqlitekonekcija.Konekcija;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.sql.*;

import static borilackaArena.sqlitekonekcija.Konekcija.kreiranjeTabele;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("fxml/pocetak.fxml"));
        primaryStage.setTitle("BORILACKA ARENA");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();


    }
    public static void main(String[] args) {

        Konekcija.konekcija();
        launch(args);
    }
}
