package borilackaArena;

import borilackaArena.borba.Borba;
import borilackaArena.borba.Heroj1;
import borilackaArena.borba.Heroj2;
import borilackaArena.sqlitekonekcija.Konekcija;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.IOException;
import static borilackaArena.borba.Borba.*;
import static borilackaArena.sqlitekonekcija.Konekcija.kreiranjeTabele;

public class IgraController {

    public static String odabranaCarolijaIgrac1 = "0", odabranaCarolijaIgrac2 = "0";
    public TextField igrac1, igrac2, borbenaArena, vrstaBorbe, izabranaCarolija1, izabranaCarolija2;
    public TextField carolija1Heroj1, carolija2Heroj1, carolija3Heroj1, carolija1Heroj2, carolija2Heroj2, carolija3Heroj2;
    public TextField nivoHeroj1, nivoHeroj2, iskustvoHeroj1, iskustvoHeroj2;
    public TextField zdravljeHeroj1, zdravljeHeroj2, snagaHeroj2, snagaHeroj1;
    Borba borba = new Borba();
    Heroj1 heroj1 = new Heroj1(borba);
    Heroj2 heroj2 = new Heroj2(borba);
    Konekcija konekcija = new Konekcija();
    public static String nacinBorbeUAreni;
    /**
     * Ova metoda se koristi za prvo osvezavanje tabele
     *
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void zapocni(ActionEvent actionEvent) throws IOException {

       // Konekcija.konekcija();
     //   kreiranjeTabele();
        String arenaName = this.borba.nazivArene();
         nacinBorbeUAreni = this.borba.nacinBorbeUAreni();
        this.borba.prikazRezultata();
        nivoHeroj1.setText(Borba.nivo1);
        nivoHeroj2.setText(Borba.nivo2);
        iskustvoHeroj1.setText(Borba.iskustvo1);
        iskustvoHeroj2.setText(Borba.iskustvo2);
        zdravljeHeroj1.setText(Borba.zdravlje1);
        zdravljeHeroj2.setText(Borba.zdravlje2);
        snagaHeroj1.setText(Borba.snaga1);
        snagaHeroj2.setText(Borba.snaga2);
        carolija1Heroj1.setText(eksponat11);
        carolija2Heroj1.setText(eksponat12);
        carolija3Heroj1.setText(eksponat13);
        carolija1Heroj2.setText(eksponat21);
        carolija2Heroj2.setText(eksponat22);
        carolija3Heroj2.setText(eksponat23);
        igrac1.setText(borba.imePrvogProtivnika);
        igrac2.setText(borba.imeDrugogProtivnika);
        borbenaArena.setText(arenaName);
        vrstaBorbe.setText(nacinBorbeUAreni);

    }

    /**
     * Ova metoda pokrece rad niti i same igre
     *
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void zapocniIgru(ActionEvent actionEvent) throws IOException {

        borba.prikazRezultata();
        nivoHeroj1.setText(Borba.nivo1);
        nivoHeroj1.setText(Borba.nivo2);
        iskustvoHeroj1.setText(Borba.iskustvo1);
        iskustvoHeroj2.setText(Borba.iskustvo2);
        zdravljeHeroj1.setText(Borba.zdravlje1);
        zdravljeHeroj2.setText(Borba.zdravlje2);
        snagaHeroj1.setText(Borba.snaga1);
        snagaHeroj2.setText(Borba.snaga2);
        carolija1Heroj1.setText(eksponat11);
        carolija2Heroj1.setText(eksponat12);
        carolija3Heroj1.setText(eksponat13);
        carolija1Heroj2.setText(eksponat21);
        carolija2Heroj2.setText(eksponat22);
        carolija3Heroj2.setText(eksponat23);
        odabranaCarolijaIgrac1 = izabranaCarolija1.getText();
        odabranaCarolijaIgrac2 = izabranaCarolija2.getText();
        heroj2.start();
        heroj1.start();
        borba.prikazRezultata();
        nivoHeroj1.setText(Borba.nivo1);
        nivoHeroj1.setText(Borba.nivo2);
        iskustvoHeroj1.setText(Borba.iskustvo1);
        iskustvoHeroj2.setText(Borba.iskustvo2);
        zdravljeHeroj1.setText(Borba.zdravlje1);
        zdravljeHeroj2.setText(Borba.zdravlje2);
        snagaHeroj1.setText(Borba.snaga1);
        snagaHeroj2.setText(Borba.snaga2);
        carolija1Heroj1.setText(eksponat11);
        carolija2Heroj1.setText(eksponat12);
        carolija3Heroj1.setText(eksponat13);
        carolija1Heroj2.setText(eksponat21);
        carolija2Heroj2.setText(eksponat22);
        carolija3Heroj2.setText(eksponat23);
        odabranaCarolijaIgrac1 = izabranaCarolija1.getText();
        odabranaCarolijaIgrac2 = izabranaCarolija2.getText();

    }

    /**
     * Ova metoda prikazuje trenutno stanje rezultata u igri
     *
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void trenutanRezultat(ActionEvent actionEvent) throws IOException {

        borba.prikazRezultata();
        nivoHeroj1.setText(Borba.nivo1);
        nivoHeroj1.setText(Borba.nivo2);
        iskustvoHeroj1.setText(Borba.iskustvo1);
        iskustvoHeroj2.setText(Borba.iskustvo2);
        zdravljeHeroj1.setText(Borba.zdravlje1);
        zdravljeHeroj2.setText(Borba.zdravlje2);
        snagaHeroj1.setText(Borba.snaga1);
        snagaHeroj2.setText(Borba.snaga2);
        carolija1Heroj1.setText(eksponat11);
        carolija2Heroj1.setText(eksponat12);
        carolija3Heroj1.setText(eksponat13);
        carolija1Heroj2.setText(eksponat21);
        carolija2Heroj2.setText(eksponat22);
        carolija3Heroj2.setText(eksponat23);
        odabranaCarolijaIgrac1 = izabranaCarolija1.getText();
        odabranaCarolijaIgrac2 = izabranaCarolija2.getText();

    }

    /**
     * Ova metoda sluzi za prelazak na sledecu stranu
     *
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void sledecaStrana(ActionEvent actionEvent) throws IOException {

   try{
       heroj1.stop();
       heroj2.stop();
       kreiranjeTabele();
       konekcija.izmenaPodataka(borbenaArena.getText(), igrac1.getText(), igrac2.getText(), nacinBorbeUAreni, borba.rezultat());
       konekcija.prikaz();
    } catch (Exception e) {
        e.getMessage();
    }


        Parent fxmlLoader = FXMLLoader.load(getClass().getResource("fxml/kraj.fxml"));
        Scene scene = new Scene(fxmlLoader, 600, 400);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
