package borilackaArena.nacinborbe;

public abstract class NacinBorbe {

    private String nacinBorbe;

    public String getNacinBorbe() {
        return nacinBorbe;
    }

    public void setNacinBorbe(String nacinBorbe) {
        this.nacinBorbe = nacinBorbe;
    }

}
