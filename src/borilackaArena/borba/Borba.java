package borilackaArena.borba;


import borilackaArena.IgraController;
import borilackaArena.NacinBorbeController;
import borilackaArena.arena.Arena;
import borilackaArena.arena.ArenaUbica;
import borilackaArena.arena.LedenaArena;
import borilackaArena.arena.OsnovnaArena;
import borilackaArena.carolija.*;
import borilackaArena.heroj.Carobnjak;
import borilackaArena.heroj.Heroj;
import borilackaArena.heroj.Ratnik;
import borilackaArena.heroj.Svestenik;
import borilackaArena.nacinborbe.Epicentar;
import borilackaArena.nacinborbe.NacinBorbe;
import borilackaArena.nacinborbe.Osnovni;

import java.util.Random;
import java.util.concurrent.Semaphore;

import static borilackaArena.ArenaController.odabranaArena;
import static borilackaArena.HerojController.odabraniHeroj;
import static java.lang.Thread.sleep;

public class Borba {

    public static String nivo1, nivo2, snaga1, snaga2, zdravlje1, zdravlje2, iskustvo1,iskustvo2, eksponat11, eksponat12, eksponat13,eksponat21,eksponat22,eksponat23 ;
    static Semaphore prviBorac = new Semaphore(0), drugiBorac = new Semaphore(1);
    private Random randomIzabranBroj = new Random();
    private Arena osnovnaArena = new OsnovnaArena(),  arenaUbica = new ArenaUbica(), ledenaArena = new LedenaArena();
    private Carolija osnovniUdarac = new OsnovniUdarac(), magicniUdarac = new MagicniUdarac(), snazniUdarac = new SnazniUdarac(), brzUdarac = new BrzUdarac();
    private NacinBorbe epicentar = new Epicentar(), osnovni = new Osnovni();
    private Heroj carobnjak = new Carobnjak(),  svestenik = new Svestenik(), ratnik = new Ratnik();
    private String nacin = nacinBorbeUAreni(), protivnik1, protivnik2;
    private int arenaZaIgrace = 0;
    public int nacinBorbeBoraca = 0;
    public String imePrvogProtivnika = nazivPrvogIgraca(), imeDrugogProtivnika = nazivDrugogIgraca();

    /**
     * Ova metoda vraca naziv arene koju je igrac odabrao
     *
     * @return (osnovnaArena ili ledenaArena ili arenaUbica)
     * default- osnovnaArena
     */
    public String nazivArene() {

        char borbenaArena = odabranaArena;
        switch (borbenaArena) {
            case '1':
                arenaZaIgrace = 1;
                return osnovnaArena.getImeArene();
            case '2':
                arenaZaIgrace = 2;
                return ledenaArena.getImeArene();
            case '3':
                arenaZaIgrace = 3;
                return arenaUbica.getImeArene();
            default:
                arenaZaIgrace = 1;
                return osnovnaArena.getImeArene();
        }
    }

    /**
     * Ova metoda vraca naziv heroja
     *
     * @return (ratnik ili svestenik ili carobnjak)
     * default- svestenik
     */
    public String nazivPrvogIgraca() {

        char izabraniPrviIgrac = odabraniHeroj;
        switch (izabraniPrviIgrac) {
            case '1':
                protivnik1 = svestenik.getNazivHeroja();
                return protivnik1;
            case '2':
                protivnik1 = ratnik.getNazivHeroja();
                return protivnik1;
            case '3':
                protivnik1 = carobnjak.getNazivHeroja();
                return protivnik1;
            default:
                return svestenik.getNazivHeroja();
        }
    }

    /**
     * Ova metoda vraca ime protivnika (random odabir)
     *
     * @return (svestenik ili carobnjak ili ratnik)
     */
    public String nazivDrugogIgraca() {
        for (; ; ) {
            int drugiRandomOdabraniIgrac = ((int) (Math.random() * 10));
            if ((protivnik1 == "Svestenik" || protivnik1 == "Ratnik") && drugiRandomOdabraniIgrac == 1) {
                protivnik2 = carobnjak.getNazivHeroja();
                return protivnik2;
            } else if ((protivnik1 == "Carobnjak" || protivnik1 == "Ratnik") && drugiRandomOdabraniIgrac == 2) {
                protivnik2 = svestenik.getNazivHeroja();
                return protivnik2;
            } else if ((protivnik1 == "Carobnjak" || protivnik1 == "Svestenik") && drugiRandomOdabraniIgrac == 3) {
                protivnik2 = ratnik.getNazivHeroja();
                return protivnik2;
            }
        }
    }

    /**
     * Ova metoda vraca nacin borbe koju je igrac izabrao
     *
     * @return (epicentar ili standardi)
     */
    public String nacinBorbeUAreni() {

        char zeljeniNacinBorbeBoraca = NacinBorbeController.odabraniNacinBorbe;
        switch (zeljeniNacinBorbeBoraca) {
            case '1':
                nacinBorbeBoraca = 1;
                return osnovni.getNacinBorbe();
            case '2':
                nacinBorbeBoraca = 2;
                return epicentar.getNacinBorbe();
            default:
                nacinBorbeBoraca = 1;
                return osnovni.getNacinBorbe();
        }
    }

    /**
     * Ova metoda stampa trenutne rezultate igraca(nivo, trenutno iskustvo, trenutno zdravlje, trenutnu snagu, carolije)
     *
     * @param heroj1 -borac 1
     * @param heroj2 -borac 2
     */
    public void stampanje(Heroj heroj1, Heroj heroj2) {

        nivo1 =  String.valueOf(heroj1.getTrenutniNivo());
        nivo2 = String.valueOf(heroj2.getTrenutniNivo());
        snaga1 =  String.valueOf(heroj1.getTrenutnaSnaga());
        snaga2 =  String.valueOf(heroj2.getTrenutnaSnaga());
        zdravlje1 = String.valueOf(heroj1.getTrenutnoZdravlje());
        zdravlje2 = String.valueOf(heroj2.getTrenutnoZdravlje());
        iskustvo1 = String.valueOf(heroj1.getTrenutnoIskustvo());
        iskustvo2 = String.valueOf(heroj2.getTrenutnoIskustvo());
        eksponat11 =  heroj1.getCarolija1().getNazivCarolije();
        eksponat12  = heroj1.getCarolija2().getNazivCarolije();
        eksponat13 = heroj1.getCarolija3().getNazivCarolije();
        eksponat21 = heroj2.getCarolija1().getNazivCarolije();
        eksponat22 = heroj2.getCarolija2().getNazivCarolije();
        eksponat23 =  heroj2.getCarolija3().getNazivCarolije();

    }

    /**
     * Ova metoda vraca random broj
     *
     * @return (broj od 0 do 40)
     */
    public int randomKriticnaSansa() {
        int max = 40;
        int min = 1;
        int kriticnaSansa = min + randomIzabranBroj.nextInt(max);
        return kriticnaSansa;
    }

    void potezPrvogIgraca() {

        try {
            sleep(2000);
            prviBorac.acquire();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        if (imePrvogProtivnika == carobnjak.getNazivHeroja()) {
            try {
                odabirCarolijeHeroja1(carobnjak);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if (imePrvogProtivnika == svestenik.getNazivHeroja()) {
            try {
                odabirCarolijeHeroja1(svestenik);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            try {
                odabirCarolijeHeroja1(ratnik);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        drugiBorac.release();
    }

    void potezDrugogIgraca() throws InterruptedException {
        try {
            sleep(2000);
            drugiBorac.acquire();
            sleep(2000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        if (imeDrugogProtivnika == carobnjak.getNazivHeroja()) {
            odabirCarolijeHeroja2(carobnjak);
        } else if (imeDrugogProtivnika == svestenik.getNazivHeroja()) {
            odabirCarolijeHeroja2(svestenik);
        } else {
            odabirCarolijeHeroja2(ratnik);
        }
        prviBorac.release();
    }

    /**
     * Ova metoda proverava kriticnu sansu i ispravlja trenutno zdravlje heroja
     *
     * @param heroj1
     * @param heroj2
     */
    public void proveraKriticneSanseHeroj1(Heroj heroj1, Heroj heroj2, int carolija) {

        if (carolija == 1) {
            if (heroj2.getTrenutnoZdravlje() >= osnovniUdarac.getMinNanesenaSteta()) {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - osnovniUdarac.getMinNanesenaSteta());
            } else if (heroj2.getTrenutnoZdravlje() >= osnovniUdarac.getMaxNanesenaSteta()) {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - osnovniUdarac.getMaxNanesenaSteta());
            }
        } else if (carolija == 2) {
            if (heroj2.getTrenutnoZdravlje() >= magicniUdarac.getMinNanesenaSteta()) {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - magicniUdarac.getMinNanesenaSteta());
            } else if (heroj2.getTrenutnoZdravlje() >= magicniUdarac.getMaxNanesenaSteta()) {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - magicniUdarac.getMaxNanesenaSteta());
            }
        } else if (carolija == 3) {
            if (heroj2.getTrenutnoZdravlje() >= snazniUdarac.getMinNanesenaSteta()) {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - snazniUdarac.getMinNanesenaSteta());
            } else if (heroj2.getTrenutnoZdravlje() >= snazniUdarac.getMaxNanesenaSteta()) {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - snazniUdarac.getMaxNanesenaSteta());
            }
        } else if (carolija == 4) {
            if (heroj2.getTrenutnoZdravlje() >= brzUdarac.getMinNanesenaSteta()) {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - brzUdarac.getMinNanesenaSteta());
            } else if (heroj2.getTrenutnoZdravlje() >= brzUdarac.getMaxNanesenaSteta()) {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - brzUdarac.getMaxNanesenaSteta());
            }
        }
        if (heroj2.getTrenutnoZdravlje() >= heroj2.getMaxSteta()) {
            heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - heroj2.getMaxSteta());

        } else if (heroj2.getTrenutnoZdravlje() >= heroj2.getMinSteta()) {
            heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - heroj2.getMinSteta());
        }
        heroj1.setTrenutnoIskustvo(heroj1.getTrenutnoIskustvo() + 4);
        nivo(heroj1);
        if (heroj1.getTrenutniNivo() == 2 || heroj1.getTrenutniNivo() == 3) {
            heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() + 7);
        }
        if (heroj1.getTrenutniNivo() == 4 || heroj1.getTrenutniNivo() == 5) {
            heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - 7);
        }
        stampanje(heroj1, heroj2);
    }

    /**
     * Ova metoda proverava kriticnu sansu i ispravlja trenutno zdravlje heroja
     *
     * @param heroj1
     * @param heroj2
     */
    public void proveraKriticneSanseHeroj2(Heroj heroj1, Heroj heroj2, int carolija) {

        if (carolija == 1) {
            if (heroj1.getTrenutnoZdravlje() >= osnovniUdarac.getMinNanesenaSteta()) {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - osnovniUdarac.getMinNanesenaSteta());
            } else if (heroj1.getTrenutnoZdravlje() >= osnovniUdarac.getMaxNanesenaSteta()) {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - osnovniUdarac.getMaxNanesenaSteta());
            }
        } else if (carolija == 2) {
            if (heroj1.getTrenutnoZdravlje() >= magicniUdarac.getMinNanesenaSteta()) {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - magicniUdarac.getMinNanesenaSteta());
            } else if (heroj1.getTrenutnoZdravlje() >= magicniUdarac.getMaxNanesenaSteta()) {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - magicniUdarac.getMaxNanesenaSteta());
            }
        } else if (carolija == 3) {
            if (heroj1.getTrenutnoZdravlje() >= snazniUdarac.getMinNanesenaSteta()) {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - snazniUdarac.getMinNanesenaSteta());
            } else if (heroj1.getTrenutnoZdravlje() >= snazniUdarac.getMaxNanesenaSteta()) {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - snazniUdarac.getMaxNanesenaSteta());
            }
        } else if (carolija == 4) {
            if (heroj1.getTrenutnoZdravlje() >= brzUdarac.getMinNanesenaSteta()) {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - brzUdarac.getMinNanesenaSteta());
            } else if (heroj1.getTrenutnoZdravlje() >= brzUdarac.getMaxNanesenaSteta()) {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - brzUdarac.getMaxNanesenaSteta());
            }
        }
        if (heroj1.getTrenutnoZdravlje() >= heroj1.getMaxSteta()) {
            heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - heroj1.getMaxSteta());
        } else if (heroj1.getTrenutnoZdravlje() >= heroj1.getMinSteta()) {
            heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - heroj1.getMinSteta());
        }
        heroj2.setTrenutnoIskustvo(heroj2.getTrenutnoIskustvo() + 4);
        nivo(heroj2);
        if (heroj2.getTrenutniNivo() == 2 || heroj2.getTrenutniNivo() == 3) {
            heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() + 7);
        }
        if (heroj2.getTrenutniNivo() == 4 || heroj2.getTrenutniNivo() == 5) {
            heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - 7);
        }
        stampanje(heroj1, heroj2);
    }


    public void proveraHeroja1(Heroj heroj, int carolija) {
        if (protivnik2 == carobnjak.getNazivHeroja()) {
            proveraKriticneSanseHeroj1(heroj, carobnjak, carolija);
        } else if (protivnik2 == svestenik.getNazivHeroja()) {
            proveraKriticneSanseHeroj1(heroj, svestenik, carolija);
        } else if (protivnik2 == ratnik.getNazivHeroja()) {
            proveraKriticneSanseHeroj1(heroj, ratnik, carolija);
        }
    }


    public void proveraHeroja2(Heroj heroj, int carolija) {
        if (protivnik1 == carobnjak.getNazivHeroja()) {
            proveraKriticneSanseHeroj2(carobnjak, heroj, carolija);
        } else if (protivnik1 == svestenik.getNazivHeroja()) {
            proveraKriticneSanseHeroj2(svestenik, heroj, carolija);
        } else if (protivnik1 == ratnik.getNazivHeroja()) {
            proveraKriticneSanseHeroj2(ratnik, heroj, carolija);
        }
    }


    public void odabirCarolijeHeroja1(Heroj heroj) throws InterruptedException {

        if (arenaZaIgrace == 2) {
            heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() - 4);
        } else if (arenaZaIgrace == 3) {
            heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() - 2);
        }

        sleep(6000);
        String spell = IgraController.odabranaCarolijaIgrac1;
        switch (spell) {
            case "1":
                proveraHeroja1(heroj, 1);
                break;
            case "2":
                if (heroj.getNazivHeroja() == svestenik.getNazivHeroja()) {
                    //You lost the move, you don't have that spell!
                    break;
                } else {
                    if (heroj.getTrenutnaSnaga() >= 4) {
                        heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                    }
                    proveraHeroja1(heroj, 2);
                    break;
                }

            case "3":
                if (heroj.getNazivHeroja() == carobnjak.getNazivHeroja()) {
                    //You lost the move, you don't have that spell!
                    break;
                } else {
                    if (heroj.getTrenutnaSnaga() >= 4) {
                        heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                    }
                    proveraHeroja1(heroj, 3);
                    break;
                }
            case "4":
                if (heroj.getNazivHeroja() == ratnik.getNazivHeroja()) {
                    //You lost the move, you don't have that spell!
                    break;
                } else if (heroj.getTrenutnaSnaga() >= 4) {
                    heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                }
                proveraHeroja1(heroj, 4);
                break;
            case "5":
                if (heroj.getTrenutnaSnaga() >= 4) {
                    heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);

                    if (heroj.getTrenutnoZdravlje() < 25 && heroj.getTrenutnoZdravlje() > 0) {
                        heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() + 5);
                    }
                } else {
                    //  System.out.print("Sorry...");
                }
                break;
            default:
                proveraHeroja1(heroj, 5);
                break;
        }
    }


    public void odabirCarolijeHeroja2(Heroj heroj) throws InterruptedException {

        if (arenaZaIgrace == 2) {
            heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() - 4);
        } else if (arenaZaIgrace == 3) {
            heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() - 2);
        }
        if (nacin == "Osnovni") {
            sleep(6000);
            int randNumber = ((int) (Math.random() * 10));
            switch (randNumber) {
                case 1:
                    proveraHeroja2(heroj, 1);
                    break;
                case 2:
                    if (heroj.getNazivHeroja() == svestenik.getNazivHeroja()) {
                        break;
                    } else {
                        if (heroj.getTrenutnaSnaga() >= 4) {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        }
                        proveraHeroja2(heroj, 2);
                        break;
                    }
                case 3:
                    if (heroj.getNazivHeroja() == carobnjak.getNazivHeroja()) {
                        break;
                    } else {
                        if (heroj.getTrenutnaSnaga() >= 4) {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        }
                        proveraHeroja2(heroj, 3);
                        break;
                    }
                case 4:
                    if (heroj.getNazivHeroja() == ratnik.getNazivHeroja()) {
                        break;
                    } else {
                        if (heroj.getTrenutnaSnaga() >= 4) {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        }
                        proveraHeroja2(heroj, 4);
                        break;
                    }
                case 5:
                    if (heroj.getTrenutnaSnaga() >= 4) {
                        heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        if (heroj.getTrenutnoZdravlje() < 25 && heroj.getTrenutnoZdravlje() > 0) {
                            heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() + 5);
                        }
                    } else {
                        // System.out.print("Sorry...");
                    }
                    break;
                default:
                    proveraHeroja2(heroj, 1);
                    break;
            }
        } else {
            sleep(6000);
            String wantSpell = IgraController.odabranaCarolijaIgrac2;
            switch (wantSpell) {
                case "1":
                    proveraHeroja2(heroj, 1);
                    break;
                case "2":
                    if (heroj.getNazivHeroja() == svestenik.getNazivHeroja()) {
                        //You lost the move, you don't have that spell!
                        break;
                    } else {
                        if (heroj.getTrenutnaSnaga() >= 4) {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        }
                        proveraHeroja2(heroj, 2);
                        break;
                    }
                case "3":
                    if (heroj.getNazivHeroja() == carobnjak.getNazivHeroja()) {
                        //You lost the move, you don't have that spell!
                        break;
                    } else {
                        if (heroj.getTrenutnaSnaga() >= 4) {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        }
                        proveraHeroja2(heroj, 3);
                        break;
                    }
                case "4":
                    if (heroj.getNazivHeroja() == ratnik.getNazivHeroja()) {
                        //You lost the move, you don't have that spell!
                        break;
                    } else {
                        if (heroj.getTrenutnaSnaga() >= 4) {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        }
                        proveraHeroja2(heroj, 4);
                        break;
                    }
                case "5":
                    if (heroj.getTrenutnaSnaga() >= 4) {
                        heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        if (heroj.getTrenutnoZdravlje() < 25 && heroj.getTrenutnoZdravlje() > 0) {
                            heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() + 5);
                        }
                    } else {
                        //System.out.print("Sorry...");
                    }
                    break;
                default:
                    proveraHeroja2(heroj, 1);
                    break;
            }
        }
    }

    /**
     * Ova metoda poziva metodu za stampanje rezultata
     */
    public void prikazRezultata() {
        if (protivnik1 == carobnjak.getNazivHeroja()) {
            carolije(carobnjak);
            if (protivnik2 == svestenik.getNazivHeroja()) {
                carolije(svestenik);
                stampanje(carobnjak, svestenik);
            } else {
                carolije(ratnik);
                stampanje(carobnjak, ratnik);
            }
        } else if (protivnik1 == svestenik.getNazivHeroja()) {
            carolije(svestenik);
            if (protivnik2 == carobnjak.getNazivHeroja()) {
                carolije(carobnjak);
                stampanje(svestenik, carobnjak);
            } else {
                carolije(ratnik);
                stampanje(svestenik, ratnik);
            }
        } else {
            carolije(ratnik);
            if (protivnik2 == carobnjak.getNazivHeroja()) {
                carolije(carobnjak);
                stampanje(ratnik, carobnjak);
            } else {
                carolije(svestenik);
                stampanje(ratnik, svestenik);
            }
        }
    }

    /**
     * Ova metoda u zavisnosti od iskstva odredjuje nivo
     *
     * @param heroj
     */
    public void nivo(Heroj heroj) {
        if (heroj.getTrenutnoIskustvo() > 10 && heroj.getTrenutnoIskustvo() <= 20) {
            heroj.setTrenutniNivo(2);
        } else if (heroj.getTrenutnoIskustvo() > 20 && heroj.getTrenutnoIskustvo() < 30) {
            heroj.setTrenutniNivo(3);
        } else if (heroj.getTrenutnoIskustvo() >= 30 && heroj.getTrenutnoIskustvo() < 40) {
            heroj.setTrenutniNivo(4);
        } else if (heroj.getTrenutnoIskustvo() >= 40 && heroj.getTrenutnoIskustvo() < 50) {
            heroj.setTrenutniNivo(5);
        }
    }

    /**
     * Ova metoda dodaje carolije igracima
     *
     * @param heroj
     */
    public void carolije(Heroj heroj) {

        if (heroj == carobnjak) {
            carobnjak.setCarolija1(osnovniUdarac);
            carobnjak.setCarolija2(magicniUdarac);
            carobnjak.setCarolija3(brzUdarac);
        } else if (heroj == svestenik) {
            svestenik.setCarolija1(osnovniUdarac);
            svestenik.setCarolija2(brzUdarac);
            svestenik.setCarolija3(snazniUdarac);
        } else {
            ratnik.setCarolija1(osnovniUdarac);
            ratnik.setCarolija2(snazniUdarac);
            ratnik.setCarolija3(magicniUdarac);
        }
    }

    /**
     *
     * This method checks and returns the winner
     *
     * @return the winner
     */
    public String rezultat() {
        String result = "";
        if (imePrvogProtivnika == "Svestenik") {
            if (imeDrugogProtivnika == "Ratnik") {
                if (svestenik.getTrenutnoZdravlje() > ratnik.getTrenutnoZdravlje()) {
                    result = "Pobednik je Svestenik";
                } else {
                    result = "Pobednik je Ratnik";
                }
            } else if ((imeDrugogProtivnika == "Carobnjak")) {
                if (svestenik.getTrenutnoZdravlje() >  carobnjak.getTrenutnoZdravlje()) {
                    result = "Pobednik je Svestenik";
                } else {
                    result = "Pobednik je Carobnjak";
                }
            }
        } else if (imePrvogProtivnika == "Ratnik") {
            if (imeDrugogProtivnika == "Svestenik") {
                if (ratnik.getTrenutnoZdravlje() > svestenik.getTrenutnoZdravlje()) {
                    result = "Pobednik je Ratnik";
                } else {
                    result = "Pobednik je Svestenik";
                }
            } else if (imeDrugogProtivnika == "Carobnjak") {
                if (ratnik.getTrenutnoZdravlje() > carobnjak.getTrenutnoZdravlje()) {
                    result = "Pobednik je Ratnik";
                } else {
                    result = "Pobednik je Carobnjak";
                }
            }
        } else {
            if (imeDrugogProtivnika == "Svestenik")  {
                if (svestenik.getTrenutnoZdravlje() > svestenik.getTrenutnoZdravlje()) {
                    result = "Pobednik je Carobnjak";
                } else {
                    result = "Pobednik je Svestenik";
                }
            } else if (imeDrugogProtivnika == "Ratnik")  {
                if (svestenik.getTrenutnoZdravlje()  > ratnik.getTrenutnoZdravlje()) {
                    result = "Pobednik je Carobnjak";
                } else {
                    result = "Pobednik je Ratnik";
                }
            }
        }
        return result;
    }
    public Heroj getCarobnjak() {
        return carobnjak;
    }

    public Heroj getSvestenik() {
        return svestenik;
    }

    public Heroj getRatnik() {
        return ratnik;
    }
}