package borilackaArena.heroj;


import borilackaArena.carolija.Carolija;

public abstract class Heroj {

    private String nazivHeroja;
    private int maxIskustvo, trenutnoIskustvo, trenutniNivo, trenutnoZdravlje, trenutnaSnaga, kriticnaSansa, maxSnaga, maxZdravlje, minSteta, maxSteta;
    private Carolija carolija1, carolija2, carolija3;
    private static int maxNivo = 5;

    public int getMinSteta() {
        return minSteta;
    }

    public void setMinSteta(int minSteta) {
        this.minSteta = minSteta;
    }

    public int getMaxSteta() {
        return maxSteta;
    }

    public void setMaxSteta(int maxSteta) {
        this.maxSteta = maxSteta;
    }

    public String getNazivHeroja() {
        return nazivHeroja;
    }

    public void setNazivHeroja(String nazivHeroja) {
        this.nazivHeroja = nazivHeroja;
    }

    public int getTrenutniNivo() {
        return trenutniNivo;
    }

    public void setTrenutniNivo(int trenutniNivo) {
        this.trenutniNivo = trenutniNivo;
    }

    public void setMaxIskustvo(int maxIskustvo) {
        this.maxIskustvo = maxIskustvo;
    }

    public void setMaxSnaga(int maxSnaga) {
        this.maxSnaga = maxSnaga;
    }

    public void setMaxZdravlje(int maxZdravlje) {
        this.maxZdravlje = maxZdravlje;
    }

    public int getTrenutnoIskustvo() {
        return trenutnoIskustvo;
    }

    public void setTrenutnoIskustvo(int trenutnoIskustvo) {
        this.trenutnoIskustvo = trenutnoIskustvo;
    }

    public int getTrenutnoZdravlje() {
        return trenutnoZdravlje;
    }

    public void setTrenutnoZdravlje(int trenutnoZdravlje) {
        this.trenutnoZdravlje = trenutnoZdravlje;
    }

    public int getTrenutnaSnaga() {
        return trenutnaSnaga;
    }

    public void setTrenutnaSnaga(int trenutnaSnaga) {
        this.trenutnaSnaga = trenutnaSnaga;
    }

    public void setKriticnaSansa(int kriticnaSansa) {
        this.kriticnaSansa = kriticnaSansa;
    }

    public Carolija getCarolija1() {
        return carolija1;
    }

    public void setCarolija1(Carolija carolija1) {
        this.carolija1 = carolija1;
    }

    public Carolija getCarolija2() {
        return carolija2;
    }

    public void setCarolija2(Carolija carolija2) {
        this.carolija2 = carolija2;
    }

    public Carolija getCarolija3() {
        return carolija3;
    }

    public void setCarolija3(Carolija carolija3) {
        this.carolija3 = carolija3;
    }

}

