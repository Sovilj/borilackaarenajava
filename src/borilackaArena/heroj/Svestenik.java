package borilackaArena.heroj;


public class Svestenik extends Heroj {

    public Svestenik() {
        this.setNazivHeroja("Svestenik");
        this.setKriticnaSansa(0);
        this.setMaxSnaga(80);
        this.setMaxZdravlje(100);
        this.setTrenutniNivo(1);
        this.setTrenutnoIskustvo(0);
        this.setTrenutnoZdravlje(100);
        this.setTrenutnaSnaga(80);
        this.setMaxIskustvo(50);
        this.setMinSteta(8);
        this.setMaxSteta(16);

    }

}
