package borilackaArena.heroj;


public class Carobnjak extends Heroj {

    public Carobnjak() {

        this.setNazivHeroja("Carobnjak");
        this.setKriticnaSansa(0);
        this.setMaxSnaga(60);
        this.setMaxZdravlje(100);
        this.setTrenutniNivo(1);
        this.setTrenutnoIskustvo(0);
        this.setTrenutnoZdravlje(100);
        this.setTrenutnaSnaga(60);
        this.setMaxIskustvo(50);
        this.setMinSteta(9);
        this.setMaxSteta(13);
    }

}