package borilackaArena;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainController {

    /**
     * Ova metoda sluzi za prelazak na sledecu stranu
     *
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void sledecaStrana(ActionEvent actionEvent) throws IOException {

        Parent fxmlLoader = FXMLLoader.load(getClass().getResource("fxml/nacinborbe.fxml"));
        Scene scene = new Scene(fxmlLoader, 600, 400);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
