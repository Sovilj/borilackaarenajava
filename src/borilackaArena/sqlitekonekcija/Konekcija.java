package borilackaArena.sqlitekonekcija;
import java.sql.*;

/**
 * Ova klasa se koristi za interakciju sa bazom podataka
 *
 * @author Sovilj Ivana
 * @since
 */
public  class Konekcija {

    /**
     * Ova metoda se koristi za kreiranje baze podataka
     *
     * @return
     *
     */
    public static Connection konekcija() {


        String url = "jdbc:sqlite:C:\\borilackaArenaJava\\Borba.db";
        Connection konekcija = null;
        try {
            konekcija = DriverManager.getConnection(url);
            System.out.println("uspesno");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return konekcija;
    }

    /**
     * Ova metoda se koristi za kreiranje tabele
     */
    public static void kreiranjeTabele() {

        String url = "jdbc:sqlite:C:\\borilackaArenaJava\\Borba.db";

        String sql = "CREATE TABLE IF NOT EXISTS bitke" +
                "(id integer PRIMARY KEY AUTOINCREMENT," +
                "nazivArene text NOT NULL, " +
                "heroj1 text NOT NULL," +
                "heroj2 text NOT NULL," +
                "nacinBorbe text NOT NULL ," +
                "rezultat text NOT NULL)";

        try (Connection conn = DriverManager.getConnection(url);
             Statement statement = conn.createStatement()) {
            statement.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void izmenaPodataka(String nazivArene, String heroj1, String heroj2, String nacinBorbe, String rezultat) {

        String sql = "INSERT INTO bitke" +
                "(nazivArene, heroj1, heroj2, nacinBorbe, rezultat) VALUES (?,?,?,?,?)";
        try (Connection conn = this.konekcija();
             PreparedStatement pStatement = conn.prepareStatement(sql)) {
            pStatement.setString(1, nazivArene);
            pStatement.setString(2, heroj1);
            pStatement.setString(3, heroj2);
            pStatement.setString(4, nacinBorbe);
            pStatement.setString(5, rezultat);
            pStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Ova metoda stampa rezultate
     */
    public void prikaz() {
        String sql = "SELECT id,nazivArene,heroj1,heroj2,nacinBorbe, rezultat FROM bitke";

        try (Connection connection = this.konekcija();
             Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while (rs.next()) {
                System.out.println(rs.getInt("id") + "\t\t" + "\u001B[32m" +
                        rs.getString("nazivArene") + "\t\t" + "\u001B[30m" +
                        rs.getString("heroj1") + "\t\t" + "\u001B[34m" +
                        rs.getString("heroj2") + "\t\t" + "\u001B[31m" +
                        rs.getString("nacinBorbe") + "\t\t" + "\u001B[30m" +
                        rs.getString("rezultat"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}

