package borilackaArena;


import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import java.io.IOException;

public class KrajController {

    /**
     * Ova metoda sluzi za zatvaranje aplikacije
     *
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void zatvoriAplikaciju(ActionEvent actionEvent){
        Platform.exit();
        System.exit(0);
    }
}
