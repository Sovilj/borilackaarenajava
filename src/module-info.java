module borilackaArenaJava {

    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens borilackaArena to javafx.fxml;
    exports borilackaArena;
}